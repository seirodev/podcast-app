from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('genres/', views.genres, name='genres'),
    # path('genres/<genre_id>', views.genres, name="genre_detail")
    # path('search/', views.search, name='search'),
]