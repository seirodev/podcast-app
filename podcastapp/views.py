from django.shortcuts import render
import requests, json

# Create your views here.
def index(request):
    best_podcast_by_genre = requests.get('https://listennotes.p.mashape.com/api/v1/best_podcasts',
        headers = {
            "X-Mashape-Key": "shhOuEtOkSmshPaY3BqOQv4aGGYHp1o8MlXjsn8oMfoX4P8Rne",
            "Accept": "application/json"
        }
    )
    by_genre = json.loads(best_podcast_by_genre.content)
    context = {
        'by_genre': by_genre
    }
    return render(request, 'podcastapp/index.html', context)

def genres(request):
    podcast_genres = requests.get('https://listennotes.p.mashape.com/api/v1/genres',
        headers = {
            "X-Mashape-Key": "shhOuEtOkSmshPaY3BqOQv4aGGYHp1o8MlXjsn8oMfoX4P8Rne",
            "Accept": "application/json"
        }
    )
    genres = json.loads(podcast_genres.content)
    context = {
        'genres': genres
    }
    return render(request, 'podcastapp/genres.html', context)

def search(request):
    podcast_search = requests.get('https://listennotes.p.mashape.com/api/v1/search',
        headers = {
            "X-Mashape-Key": "shhOuEtOkSmshPaY3BqOQv4aGGYHp1o8MlXjsn8oMfoX4P8Rne",
            "Accept": "application/json"
        }
    )
    